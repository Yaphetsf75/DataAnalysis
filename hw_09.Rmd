---
title: "Tenth Week: Principal Component Analysis and Factor Analysis"
subtitle: "PCA Stock, image, ..."
author: "AlirezaAlian 93106517"
date: "`r Sys.time()`"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
---

<div align="center">
<img  src="images/stock.jpg"  align = 'center'>
</div>



```{r warning = FALSE, message = FALSE}
library(dplyr)
library(ggplot2)
library(highcharter)
library(tidyr)
library(plyr)
library(readr)
library(ggplot2)
library("EBImage")
library(devtools)
library(tidytext)
library("BBmisc")
library("tm")
library("wordcloud")
library("stringr")
library(gutenbergr)
library(wordcloud2)
library(chron)
library("data.table")
library(ngram)

library("tm")

```

```{r warning = FALSE, message = FALSE,eval=FALSE}
sname = list.files("C:/Users/Yaphetsf/Documents/RStudio/Data3/stock_dfs/") %>% str_replace(".csv","")

spath = list.files("C:/Users/Yaphetsf/Documents/RStudio/Data3/stock_dfs/",full.names = T)

indexes = read.csv("C:/Users/Yaphetsf/Documents/RStudio/Data3/indexes.csv")

A = data.frame(sname,spath)

  read_csv(A[1,2] %>% as.String()) %>% 
  select(Date,Close) -> .data
  colnames(.data)[2] = sname[1]
  Comb = data_frame(.data)

for (i in 2:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% 
  select(Date,Close) -> .data
  colnames(.data)[2] = sname[i]
  Comb = join(Comb, .data, by = NULL , type = "full", match = "all")
}

Comb = Comb %>% arrange(Date)
Comb[-4278,-1] -> Comb


  read_csv(A[1,2] %>% as.String()) %>% 
  select(Date,Open) -> .data
  colnames(.data)[2] = sname[1]
  CombOp = data_frame(.data)

for (i in 2:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% 
  select(Date,Open) -> .data
  colnames(.data)[2] = sname[i]
  CombOp = join(CombOp, .data, by = NULL , type = "full", match = "all")
}

CombOp = CombOp %>% arrange(Date)
CombOp[-4278,-1] -> CombOp
CombOp[is.na(CombOp)] <- sum(CombOp[,-1],na.rm = T)/(nrow(CombOp)*ncol(CombOp))



  read_csv(A[1,2] %>% as.String()) %>% mutate(Gardesh = Volume*(High - Low)) %>% 
  select(Date,Gardesh) -> .data
  colnames(.data)[2] = sname[1]
  CombVol = data_frame(.data)

for (i in 2:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% mutate(Gardesh = Volume*(High - Low)) %>% 
  select(Date,Gardesh) -> .data
  colnames(.data)[2] = sname[i]
  CombVol = join(CombVol, .data, by = NULL , type = "full", match = "all")
}

CombVol = CombVol %>% arrange(Date)
CombVol[-4278,-1] -> CombVol
```


> <p dir="RTL"> 
با استفاده از داده های OHLCV شرکت های تشکیل دهنده شاخص s&p500 و همچنین داده مربوط به شاخص های اقتصادی به سوالات زیر پاسخ دهید.
</p>

***

<p dir="RTL">
۱. چه شرکتی رکورددار کسب بیشترین سود در بازه یکساله، دو ساله و پنج ساله می باشد؟ این سوال را برای بخش های مختلف مورد مطالعه قرار دهید و رکورددار را معرفی کنید. (برای این کار به ستون sector داده constituents مراجعه کنید.) برای هر دو قسمت نمودار سود ده شرکت و یا بخش برتر را رسم نمایید.
</p>

```{r eval=FALSE,warning = FALSE, message = FALSE}
myFunction <- function(x,m){
  N = nrow(x)
  M = m*365
  c = 0
  if (N < M) {
    return(c)
  }
  for (i in 1:(N-M)) {
    d = x[i+M,1] - x[i,1]
    if (d>c) {
      c = d
    }
  }
  return(c)
}
b = NULL
c = NULL
d = NULL

for (i in 1:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% 
  select(Date,Close) -> .data
  a = myFunction(.data[,2],1)[[1]]
  b = c(b,a)
  a = myFunction(.data[,2],2)[[1]]
  c = c(c,a)
  a = myFunction(.data[,2],5)[[1]]
  d = c(d,a)
}
B = data.frame(b,c,d,sname)

colnames(B) = c("OneYear","TwoYear","FiveYear","Company")
```


```{r eval=FALSE}
B %>% arrange(-OneYear) %>% head(5)
B %>% arrange(-TwoYear) %>% head(5)
B %>% arrange(-FiveYear) %>% head(5)
```

***

<p dir="RTL">
۲. یک اعتقاد خرافی می گوید خرید سهام در روز سیزدهم ماه زیان آور است. این گزاره را مورد ارزیابی قرار دهید.
</p>
```{r warning = FALSE, message = FALSE,eval=FALSE}
x = NULL
for (i in 1:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% 
  select(Date,Open,Close) -> .data
  .data %>% filter(days(Date) == 13) %>% mutate(Profit = Close - Open) %>% select(Profit) -> y
  x = c(x,y[[1]])
  
  t.test(x,mu = 0,alternative = "less")
  hist(x,breaks = 1000)
}
```

***

<p dir="RTL">
۳. رکورد بیشترین گردش مالی در تاریخ بورس برای چه روزی بوده است و چرا!!!
</p>
```{r warning = FALSE, message = FALSE, eval = FALSE}
  read_csv(A[1,2] %>% as.String()) %>% mutate(Gardesh = Volume*(High - Low)) %>% 
  select(Date,Gardesh) -> .data
  colnames(.data)[2] = "Vol"
  R = .data

for (i in 2:nrow(A)) {
  read_csv(A[i,2] %>% as.String()) %>% mutate(Gardesh = Volume*(High - Low)) %>% 
  select(Date,Gardesh) -> .data
  colnames(.data)[2] = "Vol"
  R = bind_rows(R,.data)
}
  
  R %>% group_by(Date) %>% summarise(sum(Vol)) -> R
  
  rowSums(CombVol[,-(1:2)],na.rm = T) -> sV
  CombVolV = data.frame(CombVol %>% select(Date),sV)
  CombVolV %>% View

```

<p dir="RTL">
در بحران مالی گردش مالی حداکثر میشود
2008-10-10
56446749767

</p>
***

<p dir="RTL">
۴. شاخص AAPL که نماد شرکت اپل است را در نظر بگیرید. با استفاده از رگرسیون خطی یک پیش کننده قیمت شروع (open price) بر اساس k روز قبل بسازید. بهترین انتخاب برای k چه مقداری است؟ دقت پیش بینی شما چقدر است؟
</p>

```{r warning = FALSE, message = FALSE, eval = FALSE}
read_csv("C:/Users/Yaphetsf/Documents/RStudio/Data3/stock_dfs/AAPL.csv") %>% 
  select(Date,Open) -> .data
nrap = nrow(.data)
Apple.Data = data.frame(.data[-(1:10),1][[1]],.data[-(1:10),2][[1]],
                        .data[c(-(1:9),-nrap),2][[1]],.data[c(-(1:8),-((nrap-1):nrap)),2][[1]],
                        .data[c(-(1:7),-((nrap-2):nrap)),2][[1]],.data[c(-(1:6),-((nrap-3):nrap)),2][[1]],
                        .data[c(-(1:5),-((nrap-4):nrap)),2][[1]],.data[c(-(1:4),-((nrap-5):nrap)),2][[1]],
                        .data[c(-(1:3),-((nrap-6):nrap)),2][[1]],.data[c(-(1:2),-((nrap-7):nrap)),2][[1]],
                        .data[c(-(1:1),-((nrap-8):nrap)),2][[1]],.data[-((nrap-9):nrap),2][[1]])
colnames(Apple.Data) = c("Date","mainDay","1day","2day","3day","4day",
                         "5day","6day","7day","8day","9day","10day")
```

```{r ,eval=FALSE}
fit1  = lm(mainDay ~ `1day`, data = Apple.Data)
fit2  = lm(mainDay ~ `1day`+`2day`, data = Apple.Data)
fit3  = lm(mainDay ~ `1day`+`2day`+`3day`, data = Apple.Data)
fit4  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`, data = Apple.Data)
fit5  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`, data = Apple.Data)
fit6  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`+`6day`, data = Apple.Data)
fit7  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`+`6day`+`7day`, data = Apple.Data)
fit8  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`+`6day`+`7day`+`8day`, data = Apple.Data)
fit9  = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`+`6day`+`7day`+`8day`+`9day`, data = Apple.Data)
fit10 = lm(mainDay ~ `1day`+`2day`+`3day`+`4day`+`5day`+`6day`+`7day`+`8day`+`9day`+`10day`, data = Apple.Data)


```

<p dir="RTL">
برای k=8 بهترین است.
</p>
***

<p dir="RTL">
۵. بر روی داده های قیمت شروع شرکت ها الگوریتم pca را اعمال کنید. نمودار تجمعی درصد واریانس بیان شده در مولفه ها را رسم کنید. سه مولفه اول چند درصد از واریانس را تبیین می کند؟
</p>
```{r warning = FALSE, message = FALSE, eval = FALSE}

  PCA = prcomp(CombOp[,-1], center=T, scale.=T)
  plot(summary(PCA)$importance[3,], type="l",ylab="%variance explained", xlab="nth component (decreasing order)")
  summary(PCA)$importance[3,] [3]
  
```
***

<p dir="RTL">
۶. برای هر نماد اطلاعات بخش مربوطه را از داده constituents استخراج نمایید. برای هر بخش میانگین روزانه قیمت شروع شرکت های آن را محاسبه کنید. سپس با استفاده از میانگین به دست آمده  داده ایی با چند ستون که هر ستون یک بخش و هر سطر یک روز هست بسازید. داده مربوط را با داده شاخص های اقتصادی ادغام کنید. بر روی این داده pca بزنید و نمودار biplot آن را تفسیر کنید.
</p>
```{r warning = FALSE, message = FALSE, eval = FALSE}
constituents = read.csv("C:/Users/Yaphetsf/Documents/RStudio/Data3/constituents.csv", stringsAsFactors = T)
data <- list()
for (i in 1:length(sname)){
  data[[i]] <- read.csv(paste("C:/Users/Yaphetsf/Documents/RStudio/Data3/stock_dfs/", sname[[i]], ".csv", sep = ""), stringsAsFactors = T)
}
for (i in 1:length(sname)){
  data[[i]] %>% mutate(Symbol = sname[[i]]) %>% left_join(constituents, by = c("Symbol" = "Symbol")) -> data[[i]]
}
full_data <- rbindlist(data) %>% select(Date,Open,Sector)

```



```{r warning = FALSE, message = FALSE, eval = FALSE}
full_data <- full_data %>% filter(!is.na(Sector))
full_data %>% group_by(Sector,Date) %>% summarise(avOpen = mean(Open, na.rm = T)) -> avOpent
avOpent %>% data.table::dcast(Date ~ Sector, value.var = "avOpen") -> avOpenWT
avOpenWT %>% inner_join(indexes) %>% select(-Date) -> joint
pca <- prcomp(joint, center = T, scale. = T)
biplot(pca)
```
> <p dir="RTL"> 
توضیخ ایکه مشان میدهد با مولفه ی اول pca میتوان اثرداشتن شرکت ها بر قیمت را بدست آورد به جز احیانا برای یک یا دو شرکت که موافق یا راستای مثبت PC1 نیستند.
</p>
***

<p dir="RTL">
۷. روی همه اطلاعات (OHLCV) سهام اپل الگوریتم PCA را اعمال کنید. سپس از مولفه اول برای پیش بینی قیمت شروع سهام در روز آینده استفاده کنید. به سوالات سوال ۴ پاسخ دهید. آیا استفاده از مولفه اول نتیجه بهتری نسبت به داده open price برای پیش بینی قیمت دارد؟
</p>

```{r warning = FALSE, message = FALSE, eval = FALSE}
read.csv("C:/Users/Yaphetsf/Documents/RStudio/Data3/stock_dfs/AAPL.csv",stringsAsFactors = T) %>% 
  select(-Adj.Close)-> .data
pca <- prcomp(.data[,-1], center = T, scale. = T)
pca
pca$rotation
summary(pca)

```
```{r warning = FALSE, message = FALSE, eval = FALSE}
nrap = nrow(.data)
Apple.Data.PCA = data.frame(.data[-1,1],.data[-1,2],
                        .data[-nrap,2], .data[-nrap,3],
                        .data[-nrap,4], .data[-nrap,5],
                        .data[-nrap,6])
colnames(Apple.Data.PCA) = c("Date","mainOpen","yesterOpen","yesterHigh","yesterLow","yesterClose",
                         "yesterVolume")
```

```{r warning = FALSE, message = FALSE, eval = FALSE}
Apple.Data.PCA %>% mutate(pcaReg = 0.49996468*yesterOpen + 0.49996881*yesterHigh +
                            0.49998417*yesterLow + 0.49996787*yesterClose -
                            0.01069905*yesterVolume) -> Apple.Data.PCA
fitPCA  = lm(mainOpen ~ pcaReg, data = Apple.Data.PCA)
summary(fitPCA)
```

> <p dir="RTL"> 
به نظر من خطا کمتر شده است!
</p>

***

<p dir="RTL">
۸. نمودار سود نسبی شاخص s&p500 را رسم کنید. آیا توزیع سود نرمال است؟(از داده indexes استفاده کنید.)
با استفاده از ده مولفه اول سوال پنج آیا می توانید سود و ضرر شاخص s&p500 را برای روز آينده پیش بینی کنید؟ از یک مدل رگرسیون لاجستیک استفاده کنید. درصد خطای پیش بینی را به دست آورید.
</p>

***

<p dir="RTL"> 
۹. عکسی که در ابتدای متن آمده را در نظر بگیرید. با استفاده از pca عکس را فشرده کنید. سپس نمودار حجم عکس فشرده بر حسب تعداد مولفه اصلی را  رسم کنید. بهترین انتخاب برای انتخاب تعداد مولفه ها در جهت فشرده سازی چه عددی است؟
</p>

```{r eval=FALSE}
pic = flip(readImage("E:/Study/DataAnalysis/HW/hw_09/hw_09/images/stock.jpg"))

red.weigth   = .2989; green.weigth = .587; blue.weigth  = 0.114
img = red.weigth * imageData(pic)[,,1] +
  green.weigth * imageData(pic)[,,2] + blue.weigth  * imageData(pic)[,,3]
image(img, col = grey(seq(0, 1, length = 256)))


pca.img = prcomp(img, scale=TRUE)
# Let's plot the cumulative variance of all 349 components
plot(summary(pca.img)$importance[3,], type="l",
     ylab="%variance explained", xlab="nth component (decreasing order)")
# to capture 99% of the variance, we need the first 32 components
abline(h=0.99,col="red");abline(v = 32,col="red",lty=3)
round(pca.img$sdev^2, 2)

pcs = data.frame(pca.img$x)
str(pcs)

ggplot(data = pcs, aes(x = PC1, y = PC2), label(rownames)) + geom_point() 

biplot(pca.img,cex=0.8)



pca.img = prcomp(img, scale=TRUE)
# Let's plot the cumulative variance of all 349 components
plot(summary(pca.img)$importance[3,], type="l",ylab="%variance explained",xlab="nthcomponent(decreasing order)")
# to capture 99% of the variance, we need the first 32 components

abline(h=0.99,col="red");abline(v = 112,col="red",lty=3)


chosen.components = 1:112
feature.vector = pca.img$rotation[,chosen.components]
feature.vector[1:10,1:5] # show the initial values

compact.data = t(feature.vector) %*% t(img)
dim(compact.data) # we cut lots of columns

# let's recover the data and show the approximation
approx.img = t(feature.vector %*% compact.data) 
dim(approx.img)

image(approx.img, col = grey(seq(0, 1, length = 256)))

```

***

<p dir="RTL"> 
۱۰. پنج ایده جالبی که روی داده های مالی بالا می توانستیم پیاده کنیم را بیان کنید. (ایده کافی است نیازی به محاسبه بر روی داده نیست.)
</p>

> <p dir="RTL"> 
1-تخمین حجم معاملات براساس عوامل دیگر در هر روز
2-تخمینی برای بیشترین قیمت بر اساس قیمت های روز پیش و قیمت آغازی امروز بدست آوریم و از این طریق پول بدست آوریم!!!
3-رفتار پیشبینی n مولفه ی اصلی pca در تعیین قیمت سهام اپل را بدست بیاوریم.
4-میتوان اتفاقات خاص منطقه ای در خاور میانه را بررسی کرد که چه اثری بر بازار بورس آمریکا داشته اند.
5-چه شرکت هایی در زمان رکود سود بیشینه را داشته اند.
</p>
