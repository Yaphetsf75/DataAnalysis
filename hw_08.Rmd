---
title: "Eighth Week: Text Analysis in R"
subtitle: "To be, or not to be"
author: "AlirezaAlian 93106517"
date: "`r Sys.time()`"
output:
  prettydoc::html_pretty:
    theme: cayman
    highlight: github
---

<div align="center">
<img  src="images/dickens1_1.png"  align = 'center'>
</div>

> <p dir="RTL"> 
با استفاده از بسته gutenberg داده های لازم را به دست آورید و به سوالات زیر پاسخ دهید.
</p>

```{r warning = FALSE, message = FALSE}
library(dplyr)
library(ggplot2)
library(highcharter)
library(tidyr)
library(readr)
library(ggplot2)
library(devtools)
library(tidytext)
library("BBmisc")

library("tm")
library("wordcloud")
library("stringr")
library(gutenbergr)
library(wordcloud2)

library(ngram)


ThePickwickPapers = gutenberg_download(580)
OliverTwist = gutenberg_download(730)
NicholasNickleby = gutenberg_download(967)
TheOldCuriosityShop = gutenberg_download(700)
BarnabyRudge = gutenberg_download(917)
MartinChuzzlewit = gutenberg_download(968)
DombeyandSon = gutenberg_download(821)
DavidCopperfield =gutenberg_download(766)
BleakHouse =gutenberg_download(1023)
HardTimes =gutenberg_download(786)
LittleDorrit =gutenberg_download(963)
ATaleofTwoCities = gutenberg_download(98)
GreatExpectations = gutenberg_download(1400)
OurMutualFriend = gutenberg_download(883)
TheMysteryofEdwinDrood =gutenberg_download(564)

LesMiserable = gutenberg_download(135)

```

***

<p dir="RTL">
۱. چارلز دیکنز نویسنده معروف انگلیسی بالغ بر چهارده رمان (چهارده و نیم) نوشته است. متن تمامی کتاب های او را دانلود کنید و سپس بیست لغت برتر استفاده شده را به صورت یک نمودار ستونی نمایش دهید. (طبیعتا باید ابتدا متن را پاکسازی کرده و stopping words را حذف نمایید تا به کلماتی که بار معنایی مشخصی منتقل می کنند برسید.)
</p>
```{r warning = FALSE, message = FALSE}
a = c(
ThePickwickPapers,
OliverTwist,
NicholasNickleby,
TheOldCuriosityShop,
BarnabyRudge,
MartinChuzzlewit,
DombeyandSon,
DavidCopperfield,
BleakHouse,
HardTimes,
LittleDorrit,
ATaleofTwoCities,
GreatExpectations,
OurMutualFriend,
TheMysteryofEdwinDrood,
LesMiserable
)
namesVec = c("ThePickwickPapers",
"OliverTwist",
"NicholasNickleby",
"TheOldCuriosityShop",
"BarnabyRudge",
"MartinChuzzlewit",
"DombeyandSon",
"DavidCopperfield",
"BleakHouse",
"HardTimes",
'LittleDorrit',
"ATaleofTwoCities",
"GreatExpectations",
"OurMutualFriend",
"TheMysteryofEdwinDrood",
"LesMiserable")

charlesdf = data.frame()
charleslist = list()
plots <- list()
for (i in 1:15) {

wA = a[2*i] %>% str_replace_all("\""," ") %>% 
  str_replace_all("[:punct:]"," ") %>% 
  str_split(pattern = "\\s") %>% 
  unlist() %>% 
  table %>%
  as.data.frame(strinfAsFactor = F)
colnames(wA) = c("word","count")

wA = wA %>% 
  filter(!str_to_lower(word) %in% stop_words$word) %>% 
  filter(!word %in% stop_words$word) %>% 
  filter(!str_to_upper(word) %in% stop_words$word) %>% 
  filter(str_length(word)>2) %>% 
  filter(!str_detect(word,"\\d")) %>% 
  arrange(desc(count))
charlesdf = wA
charleslist[[i]] = charlesdf


plots[[i]] = ggplot(data = wA[1:20,], aes(reorder(word, count), y = count)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("Top Words of",namesVec[i],sep=" ")) + ylab("Freq") + xlab("Words")

}
```

```{r warning = FALSE, message = FALSE}

print(plots[[1]])
print(plots[[2]])
print(plots[[3]])
print(plots[[4]])

print(plots[[5]])
print(plots[[6]])
print(plots[[7]])
print(plots[[8]])

print(plots[[9]])
print(plots[[10]])
print(plots[[11]])
print(plots[[12]])

print(plots[[14]])
print(plots[[15]])

```


***

<p dir="RTL">
۲. ابر لغات ۲۰۰ کلمه پرتکرار در رمان های چارلز دیکنز را رسم نمایید. این کار را با بسته wordcloud2 انجام دهید. برای دانلود می توانید به لینک زیر مراجعه کنید.
</p>
```{r warning = FALSE, message = FALSE}
plot = list()
for (i in 1:15) {
  plot[[i]] = wordcloud2(data = charleslist[[i]][1:200,], figPath = "E:/Study/DataAnalysis/HW/hw_08/images/dickens1_1.png", size = 1.5)

}

```

```{r warning = FALSE, message = FALSE}
print(plot[[1]])
print(plot[[2]])
print(plot[[3]])
print(plot[[4]])

print(plot[[5]])
print(plot[[6]])
print(plot[[7]])
print(plot[[8]])

print(plot[[9]])
print(plot[[10]])
print(plot[[11]])
print(plot[[12]])

print(plot[[14]])
print(plot[[15]])

```


https://github.com/Lchiffon/wordcloud2

<p dir="RTL">
 با استفاده از عکسی که در ابتدا متن آمده ابر لغاتی مانند شکل زیر رسم کنید. (راهنمایی: از ورودی figpath در دستور wordcloud2 استفاده نمایید.مثالی در زیر آورده شده است.)
</p>

<div align="center">
<img  src="images/tag-word-cloud-Che-Guevara.jpg"  align = 'center'>
</div>

***

<p dir="RTL">
۳. اسم پنج شخصیت اصلی در هر رمان دیکنز را استخراج کنید و با نموداری تعداد دفعات تکرار شده بر حسب رمان را رسم نمایید. (مانند مثال کلاس در رسم اسامی شخصیت ها در سری هر پاتر)
</p>
```{r warning = FALSE, message = FALSE}
plots <- list()

charleslistproper = list()
for (i in 1:15) {

  wA = a[2*i] %>% 
    str_replace_all("\"","") %>% 
    str_replace_all("[[:punct:]]","") %>% 
    str_split(pattern = "\\s") %>% 
    unlist() %>% 
    table() %>% 
    as.data.frame(stringsAsFactors = F)
  colnames(wA) = c("word","count")
  
  
  wA = wA %>% 
    filter(!str_to_lower(word) %in% stop_words$word) %>% 
    filter(str_length(word)>1) %>% 
    filter(!str_detect(word,"\\d")) %>% 
    arrange(desc(count)) %>% 
    mutate(proper = !word %in% str_to_lower(word)) 
  charleslistproper[[i]] = wA[1:5,]
  
  
  plots[[i]] = ggplot(data = wA[1:5,], aes(reorder(word, count), y = count)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("Top Names of",namesVec[i],sep=" ")) + ylab("Freq") + xlab("Names")


}
```

```{r warning = FALSE, message = FALSE}

print(plots[[1]])
print(plots[[2]])
print(plots[[3]])
print(plots[[4]])

print(plots[[5]])
print(plots[[6]])
print(plots[[7]])
print(plots[[8]])

print(plots[[9]])
print(plots[[10]])
print(plots[[11]])
print(plots[[12]])

print(plots[[14]])
print(plots[[15]])

```

***

<p dir="RTL">
۴.  در بسته tidytext داده ایی به نام sentiments وجود دارد که فضای احساسی لغات را مشخص می نماید. با استفاده از این داده نمودار ۲۰ لغت بر  تر negative و ۲۰ لغت برتر positive را در کنار هم رسم نمایید. با استفاده از این نمودار فضای حاکم بر داستان چگونه ارزیابی می کنید؟ (به طور مثال برای کتاب داستان دو شهر فضای احساسی داستان به ترتیب تکرر در نمودار زیر قابل مشاهده است.)
</p>

<div align="center">
<img  src="images/sentiments.png"  align = 'center'>
</div>

```{r warning = FALSE, message = FALSE}
plist = list()
nlist = list()
p = NULL
n = NULL
for (i in 1:15) {
sentiments %>% filter(lexicon == "nrc") %>% select(word, sentiment) %>%
  filter(word %in% charleslist[[1]]$word) %>% 
  filter(sentiment == "negative" | sentiment == "positive") %>% 
  select(word, sentiment) -> senti
  charleslist[[i]] %>% filter(word %in% senti$word) -> words
  senti %>% filter(word %in% words$word) -> senti
   full_join(senti, words) -> words
  words %>% group_by(sentiment) %>% summarise(count = sum(count)) ->
    book_senti
  plist[[i]] = book_senti$count[which(book_senti$sentiment == "positive")]
  nlist[[i]] = book_senti$count[which(book_senti$sentiment == "negative")]
  if(length(plist[[i]]) == 0)
    plist[[i]] = 0
  if(length(nlist[[i]]) == 0)
    nlist[[i]] = 0
}
```

```{r warning = FALSE, message = FALSE}
plots <- list()

for (i in 1:15) {
 A = data.frame(
 Lab = c("Positive","Negative"),
 Val = c(plist[[i]],nlist[[i]])
 )
 colnames(A) = c("PosOrNeg","Count")
 plots[[i]] = ggplot(data = A, aes(reorder(PosOrNeg, Count), y = Count)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("Numebr of Positive nad Negative word used in",namesVec[i],sep=" ")) + ylab("Freq") + xlab("Qual")
}
```
```{r warning = FALSE, message = FALSE}

print(plots[[1]])
print(plots[[2]])
print(plots[[3]])
print(plots[[4]])

print(plots[[5]])
print(plots[[6]])
print(plots[[7]])
print(plots[[8]])

print(plots[[9]])
print(plots[[10]])
print(plots[[11]])
print(plots[[12]])

print(plots[[14]])
print(plots[[15]])

```


***

<p dir="RTL">
۵. متن داستان بینوایان را به ۲۰۰ قسمت مساوی تقسیم کنید. برای هر قسمت تعداد لغات positive و negative را حساب کنید و سپس این دو سری زمانی را در کنار هم برای مشاهده فضای احساسی داستان رسم نمایید.
</p>
```{r warning = FALSE, message = FALSE}
A = a[32]$text
N = 337
p = NULL
n = NULL
cntr = NULL
PN = list()
for (i in 1:199) {
  tmp = A[(1+(i-1)*N):(N+(i-1)*N)]
  
  wA = tmp %>% str_replace_all("\""," ") %>% 
  str_replace_all("[:punct:]"," ") %>% 
  str_split(pattern = "\\s") %>% 
  unlist() %>% 
  table %>%
  as.data.frame(strinfAsFactor = F)
colnames(wA) = c("word","count")

  wA = wA %>% 
    filter(!str_to_lower(word) %in% stop_words$word) %>% 
    filter(!word %in% stop_words$word) %>% 
    filter(!str_to_upper(word) %in% stop_words$word) %>% 
    filter(str_length(word)>2) %>% 
    filter(!str_detect(word,"\\d")) %>% 
    arrange(desc(count))


  sentiments %>% filter(lexicon == "nrc") %>% select(word, sentiment) %>%
    filter(word %in% wA$word) %>% 
    filter(sentiment == "negative" | sentiment == "positive") %>% 
    select(word, sentiment) -> senti
    wA %>% filter(word %in% senti$word) -> words
    senti %>% filter(word %in% words$word) -> senti
     full_join(senti, words) -> words
    words %>% group_by(sentiment) %>% summarise(count = sum(count)) ->
      book_senti
    plist = book_senti$count[which(book_senti$sentiment == "positive")]
    nlist = book_senti$count[which(book_senti$sentiment == "negative")]
    if(length(plist) == 0)
      plist = 0
    if(length(nlist) == 0)
      nlist = 0
   
    p = c(p,plist)
    n = c(n,nlist)
    cntr = c(cntr,i)
    
   # B = data.frame(
   # Lab = c("Positive","Negative"),
   # Val = c(plist,nlist)
   # )
   # colnames(B) = c("PosOrNeg","Count")
   # p = ggplot(data = B, aes(reorder(PosOrNeg, Count), y = Count))
   # p + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("Numebr of Positive nad Negative word used in i'th part of",namesVec[16],sep=" ")) + ylab("Freq") + xlab("Qual")

}


tmp = A[(1+(200-1)*N):67273]
  
  wA = tmp %>% str_replace_all("\""," ") %>% 
  str_replace_all("[:punct:]"," ") %>% 
  str_split(pattern = "\\s") %>% 
  unlist() %>% 
  table %>%
  as.data.frame(strinfAsFactor = F)
colnames(wA) = c("word","count")

  wA = wA %>% 
    filter(!str_to_lower(word) %in% stop_words$word) %>% 
    filter(!word %in% stop_words$word) %>% 
    filter(!str_to_upper(word) %in% stop_words$word) %>% 
    filter(str_length(word)>2) %>% 
    filter(!str_detect(word,"\\d")) %>% 
    arrange(desc(count))

  sentiments %>% filter(lexicon == "nrc") %>% select(word, sentiment) %>%
    filter(word %in% wA$word) %>% 
    filter(sentiment == "negative" | sentiment == "positive") %>% 
    select(word, sentiment) -> senti
    wA %>% filter(word %in% senti$word) -> words
    senti %>% filter(word %in% words$word) -> senti
     full_join(senti, words) -> words
    words %>% group_by(sentiment) %>% summarise(count = sum(count)) ->
      book_senti
    plist = book_senti$count[which(book_senti$sentiment == "positive")]
    nlist = book_senti$count[which(book_senti$sentiment == "negative")]
    if(length(plist) == 0)
      plist = 0
    if(length(nlist) == 0)
      nlist = 0
    p = c(p,plist)
    n = c(n,nlist)
    cntr = c(cntr,200)
    PN = data.frame(p,n,cntr)
    colnames(PN) = c("Positive","Negative","Counter")
   # B = data.frame(
   # Lab = c("Positive","Negative"),
   # Val = c(plist,nlist)
   # )
   # colnames(B) = c("PosOrNeg","Count")
   # p = ggplot(data = B, aes(reorder(PosOrNeg, Count), y = Count))
   # p + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("Numebr of Positive nad Negative word used in i'th part of",namesVec[16],sep=" ")) + ylab("Freq") + xlab("Qual")
   
   
   p = ggplot(data = PN, aes(x=Counter,y = Positive))
   p + geom_line(stat = 'identity',na.rm = TRUE)
     
  n = ggplot(data = PN, aes(x=Counter,y = Negative))
  n + geom_line(stat = 'identity',na.rm = TRUE)
   

```

***

<p dir="RTL">
۶. ابتدا ترکیبات دوتایی کلماتی که پشت سر هم می آیند را استخراج کنید و سپس نمودار ۳۰ جفت لغت پرتکرار را رسم نمایید.
</p>
```{r warning = FALSE, message = FALSE}

ngram_2 = list()

femaleList = list()
maleList = list()
plots <- list()

for (i in 1:15) {
  a[2*i] %>%  str_replace_all("\"","") %>% 
  str_replace_all("[[:punct:]]"," ") %>% sapply(tolower) %>% ngram(n = 2,sep = " ") ->x
  yy = get.phrasetable(x)[1:2]
  colnames(yy) = c("ngramm","freq")
  plots[[i]] = ggplot(data = yy[1:30,],aes(reorder(ngramm, freq), y = freq)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("BOOK",namesVec[i],"best 2-grams",sep=" ")) + ylab("Freq") + xlab("2-gram")
  ngram_2[[i]] = get.phrasetable(x)[1:2]
  
  
  
  w = NULL  
  f = NULL
  for (j in 1:nrow(yy)) {
    if(length(str_subset(yy[j,1],"^he "))!=0){
      w = c(w,str_replace(yy[j,1],"^he[[:space:]]",""))
      f = c(f,yy[j,2])
    }
  }
  maleWords = data.frame(w,f)
  colnames(maleWords) = c("Verb","Freq")
  
  
  w = NULL  
  f = NULL
  for (j in 1:nrow(yy)) {
    if(length(str_subset(yy[j,1],"^she "))!=0){
      w = c(w,str_replace(yy[j,1],"^she[[:space:]]",""))
      f = c(f,yy[j,2])
    }
  }
  femaleWords = data.frame(w,f)
  colnames(femaleWords) = c("Verb","Freq")
  
  maleList[[i]] = maleWords
  femaleList[[i]] = femaleWords
}


```

```{r warning = FALSE, message = FALSE}

print(plots[[1]])
print(plots[[2]])
print(plots[[3]])
print(plots[[4]])

print(plots[[5]])
print(plots[[6]])
print(plots[[7]])
print(plots[[8]])

print(plots[[9]])
print(plots[[10]])
print(plots[[11]])
print(plots[[12]])

print(plots[[14]])
print(plots[[15]])

```


***

<p dir="RTL">
۷. جفت کلماتی که با she و یا he آغاز می شوند را استخراج کنید. بیست فعل پرتکراری که زنان و مردان در داستان های دیکنز انجام می دهند را استخراج کنید و نمودار آن را رسم نمایید.
</p>
```{r warning = FALSE, message = FALSE}
plots <- list()
qlots <- list()
for (i in 1:15) {
  plots[[i]] = ggplot(data = maleList[[i]][1:20,],aes(reorder(Verb, Freq), y = Freq)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("BOOK",namesVec[i],"male Verbs!",sep=" ")) + ylab("Freq") + xlab("Verb")

  qlots[[i]] = ggplot(data = femaleList[[i]][1:20,],aes(reorder(Verb, Freq), y = Freq)) + geom_bar(stat = 'identity',na.rm = TRUE) + coord_flip() + ggtitle(paste("BOOK",namesVec[i],"female Verbs!",sep=" ")) + ylab("Freq") + xlab("Verb")
}
```


```{r warning = FALSE, message = FALSE}

print(plots[[1]])
print(plots[[2]])
print(plots[[3]])
print(plots[[4]])

print(plots[[5]])
print(plots[[6]])
print(plots[[7]])
print(plots[[8]])

print(plots[[9]])
print(plots[[10]])
print(plots[[11]])
print(plots[[12]])

print(plots[[14]])
print(plots[[15]])




print(qlots[[1]])
print(qlots[[2]])
print(qlots[[3]])
print(qlots[[4]])

print(qlots[[5]])
print(qlots[[6]])
print(qlots[[7]])
print(qlots[[8]])

print(qlots[[9]])
print(qlots[[10]])
print(qlots[[11]])
print(qlots[[12]])

print(qlots[[14]])
print(qlots[[15]])

```

***

<p dir="RTL">
۸. برای کتاب های دیکنز ابتدا هر فصل را جدا کنید. سپی برای هر فصل 
1-gram, 2-gram
را استخراج کنید. آیا توزیع  N-gram
در کارهای دیکنز یکسان است؟ با رسم نمودار هم این موضوع را بررسی کنید.
</p>

***

<p dir="RTL"> 
۹. برای آثار ارنست همینگوی نیز تمرین ۸ را تکرار کنید. آیا بین آثار توزیع n-grams در بین آثار این دو نویسنده یکسان است؟
</p>

***

<p dir="RTL"> 
۱۰. بر اساس دادهایی که در تمرین ۸ و ۹ از آثار دو نویسنده به دست آوردید و با استفاده از  N-gram ها یک مدل لاجستیک برای تشخیص صاحب اثر بسازید. خطای مدل چقدر است؟ برای یادگیری مدل از کتاب کتاب الیور تویست اثر دیکنز و کتاب پیرمرد و دریا استفاده نکنید. پس از ساختن مدل برای تست کردن فصل های این کتابها را به عنوان داده ورودی به مدل بدهید. خطای تشخیص چقدر است؟
</p>

